#  Вести з консолі строку зі слів (або скористайтеся константою). Напишіть код, який визначить кількість
#  слів, в цих даних.

import string

user_input_text = input('Type your text: ')

# Не получалось по-другому очистить слова от пунктуации, и наткнулся на такую функцию в поисках решения
# и пришлось ее освоить

clear_text = user_input_text.translate(str.maketrans('', '', string.punctuation))
text_without_numbers = clear_text.translate(str.maketrans('', '', '0123456789'))

clear_text_list = text_without_numbers.split()

word_quantity = len(clear_text_list)

if word_quantity == 0:
    print('No words in your text!')
elif word_quantity == 1:
    print(f'{word_quantity} word in your text!')
else:
    print(f'{word_quantity} words in your text!')
