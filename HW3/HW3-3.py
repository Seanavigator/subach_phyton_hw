# 3. Існує ліст з різними даними,
# наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть
# механізм, який сформує новий list (наприклад lst2), який би містив всі числові змінні, які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

list_1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum', None, False]
list_2 = []
for numbers in list_1:
    if type(numbers) == int or type(numbers) == float:
        list_2.append(numbers)
        continue
    while type(numbers) == str and numbers.isdigit():
        list_2.append(numbers)
        break

print(list_2)
