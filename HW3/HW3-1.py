# 1. Зформуйте строку, яка містить певну інформацію про символ у відомому слові. Наприклад "The [значення нокра
#  символа] symbol in [тут слово] is '[символ з відповідним порядковим номером]'". Слово та номер отримайте за
#  допомогою input() або скористайтеся константою. Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol
#  in "Python" is 't' ".

word = 0

while not word:
    user_input_word = str(input('Print any word: '))

    if user_input_word.isalpha():
        word = user_input_word
    else:
        print('Input word must consist just letters!')
        continue

number = 0

while not number:
    user_input_number = input('Print any number: ')

    if user_input_number.isdigit():
        number = int(user_input_number)
    else:
        print('For input use just numbers!')
        continue

    if int(user_input_number) > len(word):
        print(f'Out of the word. In your word just {len(word)} symbols!')
        number = 0
        continue

    if int(user_input_number) == 0:
        print('0 symbol does not exist!')
        number = 0
        continue

symbol = word[number - 1:number]
if number == 1:
    print(f'The {number}st symbol in \'{word}\' is \'{symbol}\'.')
elif number == 2:
    print(f'The {number}nd symbol in \'{word}\' is \'{symbol}\'.')
elif number == 3:
    print(f'The {number}rd symbol in \'{word}\' is \'{symbol}\'.')
else:
    print(f'The {number}th symbol in \'{word}\' is \'{symbol}\'.')
