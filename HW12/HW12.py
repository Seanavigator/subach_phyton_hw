"""
Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель",
наслідувані від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд.
Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".
"""


class Vehicle:
    max_speed = 20
    capacity = 50
    type = ''

    def __init__(self, initial_speed, initial_capacity, initial_type):
        self.max_speed = initial_speed
        self.capacity = initial_capacity
        self.type = initial_type

    def info(self):
        return f'Vehicle type is {self.type} and have maximum speed is {self.max_speed} km/h and' \
               f' {self.capacity} max. capacity and'


class Automobile(Vehicle):
    car_brand = 'Toyota'

    def info(self):
        return f'Car type is {self.type} developed by \'{self.car_brand}\' company ' \
               f'with maximum speed is {self.max_speed} km/h, passenger capacity - {self.capacity} people'


class Plane(Vehicle):
    ceiling = 2500

    def info(self):
        return f'Plane type is {self.type} with cruising speed is {self.max_speed} km/h, passenger capacity - ' \
               f'{self.capacity} people with baggage and {self.ceiling}m max. ceiling.'


class Ship(Vehicle):
    summer_draft = 13.0

    def info(self):
        return f'Ship type is {self.type} with maximum sea speed is {round(self.max_speed / 1.825, 1)} knots,' \
               f' deadweight - {self.capacity} t and {self.summer_draft}m max. summer draft.'


car = Automobile(initial_speed=180, initial_capacity=5, initial_type='hatchback')
plane = Plane(initial_speed=500, initial_capacity=180, initial_type='Boeing 777')
ship = Ship(initial_speed=50, initial_capacity=50000, initial_type='Ro-Ro')
print(car.info())
print(plane.info())
print(ship.info())
