'''Напишіть функцію, що приймає два аргументи. Функція повинна
якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
якщо обидва аргументи це строки - обʼєднати в одну строку
та повернути якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
у будь-якому іншому випадку повернути кортеж з цих аргументів
'''


def types_check(data1, data2):
    if (type(data1) is float or type(data1) is int) and (type(data2) is float or type(data2) is int):
        return data1 + data2
    elif type(data1) is str and type(data2) is str:
        return f'{data1} {data2}'
    elif type(data1) is str and type(data2) is not str:
        return {data1: data2}
    else:
        return (data1, data2)


result = types_check(5, False)
print(result)

