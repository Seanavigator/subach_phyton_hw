'''
Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент,
перетворений на float. Якщо перетворити не вдається функція має повернути 0.
'''


def check_float_type(data):
    try:
        return float(data)
    except:
        other_types = 0
        return other_types


result = check_float_type({False})
print(result)
