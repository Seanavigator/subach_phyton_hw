import fire
import requests
import datetime


class Url:
    headers = {'accept': 'application/json',
               'content-type': 'application/json'}

    def get_json_nbu(self, date):
        input_date = date
        nbu_url = f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json&date={input_date}'
        try:
            response = requests.request('GET', nbu_url, headers=self.headers)
        except:
            print('Exception')
        else:
            if 300 > response.status_code >= 200 and response.headers.get('Content-Type') == \
                    'application/json; charset=utf-8' and len(str(input_date)) == 8 and isinstance(input_date, int) \
                    and response.json() != []:
                resp_json = response.json()
                print(type(len(str(input_date))))
                return resp_json
            else:
                raise TypeError('Wrong parameters format! Enter required date in format: yyyymmdd.')


class Save_Cource:
    counter = 0
    current = {}
    called_date = 0
    dt = datetime.datetime.now()

    def __init__(self, resp_json):
        for i in resp_json:
            for key, value in i.items():
                if key == 'exchangedate':
                    self.called_date = value
                if key == 'txt':
                    name = value
                elif key == 'rate':
                    rate = value
            self.current.update({name: rate})

    def write_result(self):
        today = self.dt.strftime('%d %b %Y')
        with open(f'{self.called_date}.txt', mode='at') as file:
            file.write(f'\ntoday is <{today}>\n'
                       f'required date <{self.called_date}>\n\n')
            for key, value in self.current.items():
                self.counter += 1
                file.write(f'{self.counter}. {key} - {value}\n')


if __name__ == '__main__':
    url = Url()
    output_result = Save_Cource(fire.Fire(url.get_json_nbu))
    output_result.write_result()
