"""
1. Доопрацюйте класс Point так, щоб в атрибути х та у обʼєктів цього класу можна було записати тільки числа.
Використовуйте property

2. Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
Реалізуйте перевірку даних, що пишуться в точки аналогічно до класу Line. Визначет атрибут,
що містить площу трикутника (за допомогою property). Для обчислень можна використати формулу Герона
"""

from math import dist


class Point:
    _x = 0
    _y = 0

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, new_x_coord):
        if not isinstance(new_x_coord, (int, float)):
            raise TypeError
        self._x = new_x_coord

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, new_y_coord):
        if not isinstance(new_y_coord, (int, float)):
            raise TypeError
        self._y = new_y_coord


class Triangle:
    _first_point = None
    _second_point = None
    _third_point = None

    def __init__(self, first_point_coord, second_point_coord, third_point_coord):
        self.first_point = first_point_coord
        self.second_point = second_point_coord
        self.third_point = third_point_coord

    @property
    def first_point(self):
        return self._first_point

    @first_point.setter
    def first_point(self, new_first_point):
        if not isinstance(new_first_point, Point):
            raise TypeError
        self._first_point = new_first_point

    @property
    def second_point(self):
        return self._second_point

    @second_point.setter
    def second_point(self, new_second_point):
        if not isinstance(new_second_point, Point):
            raise TypeError
        self._second_point = new_second_point

    @property
    def third_point(self):
        return self._third_point

    @third_point.setter
    def third_point(self, new_third_point):
        if not isinstance(new_third_point, Point):
            raise TypeError
        self._third_point = new_third_point

    @property
    def square(self):

        a = dist((self.first_point.x, self.first_point.y), (self.second_point.x, self.second_point.y))
        b = dist((self.second_point.x, self.second_point.y), (self.third_point.x, self.third_point.y))
        c = dist((self.third_point.x, self.third_point.y), (self.first_point.x, self.first_point.y))
        semi_perimeter = (a + b + c) / 2
        square = (semi_perimeter * (semi_perimeter - a) * (semi_perimeter - b) * (semi_perimeter - c)) ** 0.5
        return square


p1 = Point(0, 0)
p2 = Point(2, 2)
p3 = Point(5, 0)
s = Triangle(p1, p2, p3)
print(s.square)
