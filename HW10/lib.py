from random import choice
import datetime

WIN_RULES = {
    'stone': 'lizard',
    'paper': 'stone',
    'scissors': 'paper',
    'lizard': 'spock',
    'spock': 'scissors'}

WIN_RULES2 = {
    'stone': 'scissors',
    'paper': 'spock',
    'scissors': 'lizard',
    'lizard': 'paper',
    'spock': 'stone'}
"""
Two dictionary created because every key can have 2 value in current game.
"""


def get_user_choice():
    """
    The function get user choice via 'input()' and check for correct enter. While
    user input incorrect function will inform user and propose to enter his choice again
    Returns(str): return user choice

    """
    while True:
        user_choice = input('Enter your choice (stone paper scissors lizard spock): ')
        if user_choice not in WIN_RULES.keys():
            print('Enter only: stone paper scissors lizard spock')
            continue
        else:
            return user_choice


def get_computer_choice():
    """
    The function choose random computer value
    Returns (str): Random computer choice from WIN_RULES dictionary

    """
    computer_choice = choice(list(WIN_RULES.keys()))
    return computer_choice


def get_winner(user_choice, computer_choice):
    """
    The function compares user choice and computer choice and return a result
    as per game's rules
    Args:
        user_choice (str): received choice from user input
        computer_choice (str): random choice from computer

    Returns (str): result win as per game's rules.
                 If user choice will equals computer choice return 'Draw'
                 If function will find in WIN_RULES or WIN_RULES2 key(user choice) and value(computer choice)
                 - user is winner.
                 In all others cases computer is winner

    """
    if user_choice == computer_choice:
        msg = 'Draw'
    elif WIN_RULES[user_choice] == computer_choice:
        msg = 'Player'
    elif WIN_RULES2[user_choice] == computer_choice:
        msg = 'Player'
    else:
        msg = 'AI'

    return msg


def make_message(user_choice, computer_choice, winner):
    """
    Te function fabricate result text after every choice
    Args:
        user_choice (str): received choice from user input
        computer_choice (str): random choice from computer
        winner (str): result comparisons user choice and computer choice
    Returns: if result of game is Draw function will return string
        User choice is {user_choice}, PC choice is {computer_choice}. Nobody won because is Draw!
           in all other cases will return string
           User choice is {user_choice}, PC choice is {computer_choice}. Winner is {winner}!

    """
    if winner == 'Draw':
        msg = f'User choice is {user_choice}, PC choice is {computer_choice}. Nobody won because is {winner}!'
    else:
        msg = f'User choice is {user_choice}, PC choice is {computer_choice}. Winner is {winner}!'
    return msg


def write_game_log(user, computer, winner):
    """
    The function write and save time and results of the game in the file "Game_Log.txt"
    Args:
        user (str): user choice
        computer (str): computer choice
        winner (str): winner in the game

    Returns: No returns

    """
    dt = datetime.datetime.now()
    with open('Game_Log.txt', mode='at') as file:
        file.write(dt.strftime('%d %b %Y %H-%M-%S') + f' Player - {user}, AI - {computer}, Winner - {winner}\n')

