"""
Візьміть своє рішення HW8 (гра) і доопрацюйте його таким чином, щоб програма записувала в файл результати гри
зі збереженням історичних результатів. Файл має містити пронумеровані у правильному порядку строки з датою та часом,
фігурами гравця та АІ і вказанням переможця. У файлі має зберігатися вся історія гри за весь час використання програми.


Приклад вмісту файлу

01 Apr 2022 20-31-33 Player - Stone, AI - Lizard, Winner - Player
09 May 2022 21-30-57 Player - Lizard, AI - Spock, Winner - Player
09 May 2022 21-32-15 Player - Paper, AI - Scissors, Winner - AI
10 May 2022 10-18-03 Player - Paper, AI - Scissors, Winner - AI
10 May 2022 10-10-00 Player - Paper, AI - Spock, Winner - Player
"""
import lib


def main():
    """
    The main function 3 times will ask user to choice one of the options
    Returns: will print result of choices to screen

    """
    for i in range(1, 4):
        print(f'Try {i}')
        user_choice = lib.get_user_choice()

        computer_choice = lib.get_computer_choice()

        winner = lib.get_winner(user_choice, computer_choice)

        msg = lib.make_message(user_choice, computer_choice, winner)

        score_log = lib.write_game_log(user_choice, computer_choice, winner)

        print(msg)


main()
