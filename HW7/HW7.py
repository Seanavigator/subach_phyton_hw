"""
Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
Попросіть користувача ввести свій вік.
- якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
- якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
- якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
- якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!)
    - вивести "О, вам <>! Який цікавий вік!"
- у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"

Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
Наприклад :
"Тобі ж 5 років! Де твої батьки?"
"Вам 81 рік? Покажіть пенсійне посвідчення!"
"О, вам 33 роки! Який цікавий вік!"

Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг.
Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.
"""


def get_number(data=0):
    """
     The function receive data from user and make checking for correct input age using just numbers.
    In case of incorrect input (letters, dots etc.) or age > 1 or > 130 user will be informed that age entered
    incorrectly and will propose to enter age again. This cycle will continue until will be received correct Age.

    Args:
        data (int): by default it will be equal 0 for creation cycle which will exclude all incorrect data

    Returns: correct data while will be received correct number from user

    """
    while not data:
        user_input = input('Будь ласка, введіть ваш вік: ')
        if user_input.isdigit():
            data = int(user_input)
        else:
            print('Ващ вік необхідно вводити лише цифрами - без літер, крапок та пробілів тощо!')
            continue
        if data < 1:
            print('Ваш вік не може бути менше 1 року!')
            continue
        if data > 130:
            print('Нажаль люди стільки не живуть. Будь ласка, використовуйте ваш справжній вік!')
            data = 0
            continue
        if int(user_input[0]) == 0:
            print('Некоректній ввід даних. Будь ласка, введіть ваш вік без нуля на початку.')
            data = 0

    return data


age_from_user = str(get_number())


def get_declension_years(arg):
    """
    The function receive user age and output the correct case ukrainian word "рік" depending on user age.
    Args:
        arg (str): received data from user in sting format for comparison by indexing, number

    Returns (str): If age in range from 11 to 19 - output declension will be "років". In all others cases will
            take last value by index "-1" in argument and compare with keys in ukr_declension_dict and return
            appropriate value.

    """
    ukr_declension_dict = {0: 'років',
                           1: 'рік',
                           2: 'роки',
                           3: 'роки',
                           4: 'роки',
                           5: 'років',
                           6: 'років',
                           7: 'років',
                           8: 'років',
                           9: 'років'}

    if 10 < int(arg) < 20:
        return 'років'
    else:
        for number, declension in ukr_declension_dict.items():
            if int(arg[-1]) == number:
                return declension


inflected_word = get_declension_years(age_from_user)


def get_correct_string(number, declension):
    """
    The function depend of user input data will return information for compare age.
    Args:
        number (str): received number from user in string formal for further comparison
                      by index
        declension(str): received correct inflected word depend of user age

             <> - arguments {number} {declension}
    Returns(str): 1. If quantity of same numbers of first symbol in argument 'number' will be
                    equals of quantity symbols in argument 'number' and quantity of symbols more
                    then 1 return --> "О, вам <>! Який цікавий вік!"
                  2. If argument 'number' < 7 return --> "Тобі ж <>! Де твої батьки?"
                  3. If argument 'number' < 16 return --> "Тобі лише <>, а це е фільм для дорослих!"
                  4. If argument 'number' > 65 return --> "Вам <>? Покажіть пенсійне посвідчення!"
                  5. In all other cases return --> "Незважаючи на те, що вам <>, білетів всеодно нема!"

    """
    if len(number) == number.count(number[0]) and len(number) > 1:
        return f'О, вам {number} {declension}! Який цікавий вік!'
    elif int(number) < 7:
        return f'Тобі ж {number} {declension}! Де твої батьки?'
    elif 7 >= int(number) < 16:
        return f'Тобі лише {number} {declension}, а це е фільм для дорослих!'
    elif int(number) > 65:
        return f'Вам {number} {declension}? Покажіть пенсійне посвідчення!'
    else:
        return f'Незважаючи на те, що вам {number} {declension}, білетів все одно нема!'


output_result = get_correct_string(age_from_user, inflected_word)
print(output_result)
