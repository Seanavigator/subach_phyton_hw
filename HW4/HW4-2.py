'''
 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну.
    Є Dict з назвами магазинів і цінами:
    {"cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
    "buy.now": 38.324, "x-store": 37.166,
    "the_partner": 38.988, "sota": 37.720, "rozetka": 38.003}.
     Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають
     в діапазон між мінімальною і максимальною ціною. Наприклад:

  lower_limit = 35.9
  upper_limit = 37.339
  > match: "x-store", "main-service"
'''

while True:
    try:
        lower_limit = float(input('Input lower limit: '))
    except:
        print('Use just digits for input lower_limit. Example: "5", "5.0"!')
        continue
    if lower_limit < 0:
        print('Limit can\'t be negative!')
        continue
    else:
        break


while True:
    try:
        upper_limit = float(input('Input upper limit: '))
    except:
        print('Use just digits for input lower_limit. Example: "5", "5.0"!')
        continue
    if upper_limit < 0:
        print('Limit can\'t be negative!')
        continue
    if upper_limit > lower_limit:
        print('Upper limit can\'t be more then lower limit!')
        continue
    else:
        break

store_dict = {"cito": 47.999,
              "BB_studio": 42.999,
              "momo": 49.999,
              "main-service": 37.245,
              "buy.now": 38.324,
              "x-store": 37.166,
              "the_partner": 38.988,
              "sota": 37.720,
              "rozetka": 38.003}

filter_shops_result = []
for prices in store_dict:
    if upper_limit < store_dict[prices] < lower_limit:
        filter_shops_result.append(prices)

if not filter_shops_result:
    print('No results found as per your request!')
else:
    print('Search_result: ' + ', '.join(filter_shops_result))
