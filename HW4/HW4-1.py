'''
 1. Дана довільна строка. Напишіть код, який знайде в ній
  і виведе на екран кількість слів,
   які містять дві голосні літери підряд.
'''

text_list = 'Quantity word in your text'.split()
vowels = tuple('aeyuioэєїё')

words_counter = 0

for word in text_list:
    temp = 0
    for letter in word:
        if letter in vowels:
            temp += 1
        else:
            temp = 0

        if temp == 2:
            words_counter += 1
            continue


print(f'Quantity word in your text with two '
      f'consecutive vowel letters is {words_counter}!')
