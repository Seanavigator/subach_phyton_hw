"""
Доопрацюйте класс Triangle таким чином, щоб він підтримував інтерфейс ітератора.
Ітератор повинен віддавати сторони трикутника (обʼєкт класу Line)
"""

from math import dist


class Point:
    _x = 0
    _y = 0

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, new_x_coord):
        if not isinstance(new_x_coord, (int, float)):
            raise TypeError
        self._x = new_x_coord

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, new_y_coord):
        if not isinstance(new_y_coord, (int, float)):
            raise TypeError
        self._y = new_y_coord


class Line:
    _begin = None
    _end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, new_begin_point):
        if not isinstance(new_begin_point, Point):
            raise TypeError
        self._begin = new_begin_point

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, new_end_point):
        if not isinstance(new_end_point, Point):
            raise TypeError
        self._end = new_end_point

    @property
    def length(self):

        k1 = (self.begin.x - self.end.x) ** 2
        k2 = (self.begin.y - self.end.y) ** 2
        res = (k1 + k2) ** 0.5

        return res


class Triangle:
    _first_line = None
    _second_line = None
    _third_line = None
    _index = 0
    _line = []

    def __init__(self, line_1, line_2, line_3):
        self.first_line = line_1
        self.second_line = line_2
        self.third_line = line_3

    def __iter__(self):
        self._line = [getattr(self, name) for name in dir(self) if (not name.startswith('_')
                      and not name.startswith('square'))]
        return self

    def __next__(self):
        try:
            result = self._line[self._index]
        except IndexError:
            raise StopIteration
        else:
            self._index += 1
            return result

    @property
    def first_line(self):
        return self._first_line

    @first_line.setter
    def first_line(self, new_line_1):
        if not isinstance(new_line_1, Line):
            raise TypeError
        self._first_line = new_line_1.length

    @property
    def second_line(self):
        return self._second_line

    @second_line.setter
    def second_line(self, new_line_2):
        if not isinstance(new_line_2, Line):
            raise TypeError
        self._second_line = new_line_2.length

    @property
    def third_line(self):
        return self._third_line

    @third_line.setter
    def third_line(self, new_line_3):
        if not isinstance(new_line_3, Line):
            raise TypeError
        self._third_line = new_line_3.length

    @property
    def square(self):
        semi_perimeter = (self.first_line + self.second_line + self.third_line) / 2
        res = (semi_perimeter * (semi_perimeter - self.first_line) * (semi_perimeter - self.second_line) *
               (semi_perimeter - self.third_line)) ** 0.5
        return res


p1 = Point(0, 0)
p2 = Point(2, 2)
p3 = Point(5, 0)
line_a = Line(p1, p2)
line_b = Line(p2, p3)
line_c = Line(p3, p1)
triangle = Triangle(line_a, line_b, line_c)
print('Square -->', triangle.square)
for line in triangle:
    print('line -->', line)


