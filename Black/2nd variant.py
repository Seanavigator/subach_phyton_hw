class Point:
    _x = 0
    _y = 0

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, new_x_coord):
        if not isinstance(new_x_coord, (int, float)):
            raise TypeError
        self._x = new_x_coord

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, new_y_coord):
        if not isinstance(new_y_coord, (int, float)):
            raise TypeError
        self._y = new_y_coord


class Line:
    _begin = None
    _end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, new_begin_point):
        if not isinstance(new_begin_point, Point):
            raise TypeError
        self._begin = new_begin_point

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, new_end_point):
        if not isinstance(new_end_point, Point):
            raise TypeError
        self._end = new_end_point

    @property
    def length(self):

        k1 = (self.begin.x - self.end.x) ** 2
        k2 = (self.begin.y - self.end.y) ** 2
        res = (k1 + k2) ** 0.5

        return res


class Triangle:
    _side1 = None
    _side2 = None
    _side3 = None

    def __init__(self, side1_a, side2_b, side3_c):
        self.side1 = side1_a
        self.side2 = side2_b
        self.side3 = side3_c

    @property
    def side1(self):
        return self._side1

    @side1.setter
    def side1(self, new_side1):
        if not isinstance(new_side1, Line):
            raise TypeError
        self._side1 = new_side1

    @property
    def side2(self):
        return self._side2

    @side2.setter
    def side2(self, new_side2):
        if not isinstance(new_side2, Line):
            raise TypeError
        self._side2 = new_side2

    @property
    def side3(self):
        return self._side3

    @side3.setter
    def side3(self, new_side3):
        if not isinstance(new_side3, Line):
            raise TypeError
        self._side3 = new_side3

    @property
    def square(self):
        semi_perimeter = (self.side1 + self.side2 + self.side3) / 2
        res = (semi_perimeter * (semi_perimeter - self.side1) * (semi_perimeter - self.side2) *
               (semi_perimeter - self.side3)) ** 0.5
        return semi_perimeter


p1 = Point(0, 0)
p2 = Point(2, 2)
p3 = Point(5, 0)
line_a = Line(p1, p2)
line_b = Line(p2, p3)
line_c = Line(p3, p1)
a = line_a.length
b = line_b.length
c = line_c.length


s = Triangle(a, b, c)
print(dir(Triangle))
print(s.square)
