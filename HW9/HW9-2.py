"""
Напишіть декоратор, який перетворює всі вхідні параметри функції в строки (str),
 а також перетворює результат роботи функції на строку
"""


def type_change_decorator(function):
    """
    Decorator change input data type to string type and output result of function to string type
    Args:
        function: just example like num1 + num2

    Returns (str): result of function num1 + num2 in string format

    """

    def wrapper(*args, **kwargs):
        new_args = tuple([str(arg) for arg in args])
        new_kwargs = {key: str(value) for key, value in kwargs.items()}
        result = str(function(*new_args, **new_kwargs))
        return result
    return wrapper


def addition(num1, num2):
    """
    Example function. addition of 2 numbers or strings
    Args:
        num1: first data
        num2: second data

    Returns: result of num1 + num2

    """
    if type(num1) == type(num2):
        result = num1 + num2
        return result
    else:
        print('Invalid types. Input same types for both value!')


addition = type_change_decorator(addition)
print(addition((), num2=6))
