"""
Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах.
"""

import time


def calculator(num1, num2, symbol):
    """
    Easy calculator for easy arithmetics operations
    Args:
        num1 (int): first number
        num2 (int): second number
        symbol(str): arithmetic symbol (+,-,*,/)

    Returns: if symbol = + --> num1 + num2
             if symbol = - --> num1 - num2
             if symbol = * --> num1 * num2
             if symbol = / --> num1 / num2
             if num2 = 0 --> 'Division by zero is impossible!'
             in all other cases --> 'Unknown operation!'
    """
    if symbol == '+':
        return num1 + num2
    elif symbol == '-':
        return num1 - num2
    elif symbol == '*':
        return num1 * num2
    elif symbol == '/':
        return num1 / num2
    elif num2 == 0:
        return 'Division by zero is impossible!'
    else:
        return 'Unknown operation!'


def get_correct_value(function):
    """
    Decorator check for correct incoming types
    Args:
        function: arguments passed by calculation function

    Returns: if num1 and num2 not in int or float types --> Numbers with wrong type!
             if  symbol not in string type --> Symbol not in string format

    """
    def wrapper(*args, **kwargs):
        for i in args:
            if not isinstance(i, (int, float)):
                raise TypeError('Numbers with wrong type!')
        for key, value in kwargs.items(): # Данная проверка тут немного безполезна, вставил просто для своего усваивания
            if type(value) != str:        # чтобы поиграться с args, kwargs
                raise TypeError('Symbol not in string format')
        result = function(*args, **kwargs)
        return result

    return wrapper


def function_time(function):
    """
    Decorator calculate function execution time
    Args:
        function: arguments passed by calculation function

    Returns: function execution time

    """
    def wrapper(*args, **kwargs):
        time_now = time.time()
        result = function(*args, **kwargs)
        print('function execution time --->', time.time() - time_now, 'sec.')
        return result

    return wrapper


calculator = get_correct_value(function_time(calculator))
print('Result --->', calculator(10, 5, symbol='True'))
