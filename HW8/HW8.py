"""
Доопрацюйте гру з заняття таким чином, щоб вона відповідала розширеним правилам (пояснення).
Винесіть усі допоміжні функції (окрім main) в окремий файл, (нехай буде lib.py) і виконайте імпорт цих функцій.
"""

import lib


def main():
    """
    The main function 3 times will ask user to choice one of the options
    Returns: will print result of choices to screen

    """
    for i in range(1, 4):
        print(f'Try {i}')
        user_choice = lib.get_user_choice()

        computer_choice = lib.get_computer_choice()

        winner = lib.get_winner(user_choice, computer_choice)

        msg = lib.make_message(user_choice, computer_choice, winner)

        print(msg)


main()
